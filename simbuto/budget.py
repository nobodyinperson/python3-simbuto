# system modules
import logging

# internal modules

# external modules
from class_tools.decorators import has_property
from class_tools.propertyobject import PropertyObject

logger = logging.getLogger(__name__)


@has_property(
    name="name",
    type=str,
    default=lambda: _("unnamed budget"),
    doc=_("This budget's name"),
)
@has_property(
    name="transactions",
    type=list,
    default=list,
    set_default=True,
    doc=_("The list of transactions"),
)
class Budget(PropertyObject):
    """
    Class holding a budget
    """
