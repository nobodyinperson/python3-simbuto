
Installation
============

:mod:`simbuto` is best installed via :mod:`pip`::

    python3 -m pip install --user simbuto
