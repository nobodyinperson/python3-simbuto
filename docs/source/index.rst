Welcome to simbuto's documentation!
===========================================

:mod:`simbuto` is a simple budgeting tool.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
