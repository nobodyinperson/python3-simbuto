# system modules
import logging

# internal modules

# external modules
from class_tools.decorators import has_property
from class_tools.propertyobject import PropertyObject

logger = logging.getLogger(__name__)


@has_property(
    name="name",
    type=str,
    default=lambda: _("unnamed transaction"),
    doc=_("This transaction's name"),
)
class Transaction(PropertyObject):
    """
    Class holding a transaction
    """
