# system modules
import logging

# internal modules

# external modules
import click

logger = logging.getLogger(__name__)


@click.group(
    help="{} - {}".format(
        _("SimBuTo").title(), _("a simple budgeting tool").title()
    ),
    context_settings={"help_option_names": ["-h", "--help"]},
    chain=True,
    invoke_without_command=True,
)
@click.option("-q", "--quiet", help=_("only show warnings"), is_flag=True)
@click.option("-v", "--verbose", help=_("verbose output"), is_flag=True)
@click.version_option(help=_("show version and exit"))
@click.pass_context
def cli(ctx, quiet, verbose):
    # set up logging
    loglevel = logging.DEBUG if verbose else logging.INFO
    loglevel = logging.WARNING if quiet else loglevel
    logging.basicConfig(
        level=loglevel, format="[%(asctime)s] - %(levelname)-8s - %(message)s"
    )
    for n, l in logger.manager.loggerDict.items():
        if not n.startswith("simbuto"):
            l.propagate = False
    logger.warning(_("Currently, this program can't do anything."))
