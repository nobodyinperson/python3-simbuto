# Simbuto - A Simple Budgeting Tool

[![pipeline status](https://gitlab.com/nobodyinperson/python3-simbuto/badges/master/pipeline.svg)](https://gitlab.com/nobodyinperson/python3-simbuto/commits/master)
[![coverage report](https://gitlab.com/nobodyinperson/python3-simbuto/badges/master/coverage.svg)](https://nobodyinperson.gitlab.io/python3-simbuto/coverage-report/)
[![documentation](https://img.shields.io/badge/docs-sphinx-brightgreen.svg)](https://nobodyinperson.gitlab.io/python3-simbuto/)
[![PyPI](https://badge.fury.io/py/simbuto.svg)](https://badge.fury.io/py/simbuto)

`simbuto` is a simple budgeting tool. This project attempts to port the
[legacy version](https://gitlab.com/nobodyinperson/simbuto) to use Python for
everything.

## What can `simbuto` do?

This version of `simbuto` can't currently do anything useful. Try out the
[legacy version](https://gitlab.com/nobodyinperson/simbuto) for some basic
budgeting functionality.


## Installation

The `simbuto` package is best installed via `pip`. Run from anywhere:

```bash
python3 -m pip install --user simbuto
```

This downloads and installs the package from the [Python Package
Index](https://pypi.org).

You may also install `simbuto` from the repository root:

```bash
python3 -m pip install --user .
```

## Translations

Currently, the following languages are available:

- English
- German

If you are interested in adding another language, just [open a New
Issue](https://gitlab.com/nobodyinperson/python3-simbuto/issues/new) and we
will get your going.

## Documentation

Documentation of the `simbuto` package can be found [here on
GitLab](https://nobodyinperson.gitlab.io/python3-simbuto/).

Also, the command-line help page `python3 -m simbuto -h` is your friend.
