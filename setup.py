#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# System modules
import os
import sys
import runpy
from setuptools import setup, find_packages


def read_file(filename):
    with open(filename, errors="ignore") as f:
        return f.read()


package = find_packages(exclude=["tests"])[0]

# run setup
setup(
    name=package,
    description="Simple Budgeting Tool",
    author="Yann Büchau",
    author_email="nobodyinperson@gmx.de",
    keywords="",
    license="GPLv3",
    version=runpy.run_path(os.path.join(package, "version.py")).get(
        "__version__", "0.0.0"
    ),
    url="https://gitlab.com/nobodyinperson/python3-simbuto",
    long_description=read_file("README.md"),
    long_description_content_type="text/markdown",
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
    ],
    install_requires=["click>=7.0", "class-tools==0.1.1"],
    tests_require=[],
    extras_require={},
    test_suite="tests",
    packages=[package],
    package_data={"simbuto.locale": ["*.mo"]},
    include_package_data=True,
    entry_points={
        "console_scripts": ["simbuto = simbuto.cli.commands.main:cli"]
    },
)
